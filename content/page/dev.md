---
title: dev
subtitle: this is an area where I might talk about everything development and technology related
---
# open source projects

<sub>

|**name and source code**   |**tech stack**    |
|---------------------------|------------|
|[*gtk-nlp-finance-vanguard*](https://gitlab.com/gkama/gtk-nlp-finance-vanguard)|*C#, ASP.NET Core 2.2, GraphQL, Docker*   |
|[*gtk-nlp-finance-vanguard*](https://gitlab.com/gkama-go/gtk-nlp-finance-vanguard-go)|*Go, Gin* |
|[*gtk-nlp-finance*](https://gitlab.com/gkama/gtk/tree/master/gkama.nlp.finance)|*C#, ASP.NET Core 2.2, MongoDb, Docker, Azure Linux App Service*  |
|[*gtk-graphql*](https://gitlab.com/gkama/gtk-graphql)|*C#, ASP.NET Core 2.2, GraphQL, Docker*   |
|[*gtk-survey*](https://gitlab.com/gkama/gtk-survey)|*C#. ASP.NET Core 2.2, GraphQL, EF Core, Docker*  |
|[*gtk-time-logging*](https://gitlab.com/gkama-go/gtk-time-logging)|*Go, Gin, GraphQL*    |
|[*gtk-sqs*](https://gitlab.com/gkama/gtk-sqs)|*C#, ASP.NET Core 2.2, AWS SQS*    |
|[*gkamacharov.com*](https://gkamacharov.com)|*GitLab Pages, GitLab CI/CD, Lets Encrypt, Hugo, Markdown* |
|[*api.gkamacharov.com*](https://api.gkamacharov.com)|*C#, ASP.NET Core 2.2, MongoDb, OpenAPI, Auth with JWT*   |

</sub>


### [nlp-finance-vanguard](https://gitlab.com/gkama/gtk-nlp-finance-vanguard)

a Natural Language Processing (nlp) API specifically for financial `Vanguard` (https://about.vanguard.com/) words and phrases. it's used to text mine and get categorizations, with weight, back

- `ASP.NET Core 2.2 API` implementation
- `GraphQL` with `ui/playground` implementation
- `vanguard_model` is stored as a static object in the code, which allows the application to be fully code-first-and-only managed
- `/categorize` endpoint that categorizes the requested text content
- `Docker` implementation with very easy image use `docker pull gkama/nlp-finance-vanguard`

### [api.gkamacharov.com](https://api.gkamacharov.com)

is my main API portal where I (might) host APIs I'm currently working on

### [gkama.graph.ql](https://gitlab.com/gkama/gtk-graphql)

sample project for `GraphQL` for `.NET Core`. you can clone the repo and run it fully locally. everything is setup using entity framework core, GraphQL and appropriate launch settings - you simply run it with the `gkama.graph.ql.core` startup project and it auto directs to the `ui/playground` endpoint

if you'd like to test run it before you clone the code, you can get the docker image from `docker pull gkama/dotnet:graphql`. once you have that, run the image and go to `localhost:[]/ui/playground` to check it out

### [gkama.nlp.finance](https://gitlab.com/gkama/gtk/tree/master)

the high level overview is that the project is a natural language processing (nlp) analyzer for financial words and phrases. you pass in text content and the engine analyzes it and returns the categories it finds. this is useful to mine text content to find what the text is talking about, in terms of financial lingo. this project is currently live at my main API portal as part of a bigger set of APIs

there is a more in depth documentation of the API in the source code and a sample request 
at the portal `https://api.gkamacharov.com`

more about the back end - it uses mongodb as the database, c#, asp.net core 2.2 API inftrastructure, and it's fully hosted on microsoft azure app service (linux) as a SaaS piece, and application insight as a logging and analytics service