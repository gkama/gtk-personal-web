---
title: career
subtitle: my career
---
### procter & gamble (p&g) (2019 - present)

contracting at [Procter & Gamble (P&G)](https://us.pg.com/) as a data engineer and back end engineer

I'm part of the CRM transformation project as a data engineer and part of the modernization of the infrastructure project as a back end engineer

*back end engineer*

- C#/Python
- ASP.NET Core
- Microservices/APIs
- Segment, Lytics
- GCP, BigQuery, Cloud Storage
- Microsoft Azure
- Handling flat, and JSON files
- Agile development methodology, DevOps and CI/CD
- IDE: Visual Studio 2019, Visual Studio Code

*data engineer*

- SQL
- GCP
- BigQuery
- Teradata/SQL Assistant
- Working with data structure, algorithms, distributed storage, database systems (SQL), data warehouse and data analytics technologies


### concentrix (2014 - 2019)

I was part of the backend engineering team on the [concentrix](https://www.concentrix.com/) cx's analytics platform

[concentrix cx](https://www.concentrix.com/resources/thought-leadership/cx-program-success-the-real-role-of-voc-software/) platform is a survey analytics portal. our clients' users range anywhere from agents to CEOs. PaaS (platform as a service) and IaaS (infrastructure as a service) architecture

I was responsible for delivering high quality and performing code with the following technologies:

- C#
- .NET Core 2.1, 2.2
- ASP .NET Core (API, MVC and Razor Pages) 2.1, 2.2
- Entity Framework Core
- Docker
- Microsoft Azure
- Microsoft Azure Cosmos DB (Table, SQL API's), Service Bus, Logic Apps, Azure Functions, Key Vault, Application Insights, Virtual Machines, Virtual Gateways, and more
- Azure DevOps
- Agile Scrum/Kanban methodologies with 3 weeks sprints. although, towards the end of 2018, we started getting away from sprints as they weren't working for us that great
- IDE: Visual Studio 2019/2017 Enterprise. comes with $150/month Microsoft Azure credits which I use for personal projects