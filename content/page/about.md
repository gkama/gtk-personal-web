---
title: about me
subtitle: what you'd like to know
---
### professional

strong C#/.NET Core agile engineer with more than 5 years of professional experience. specializing in high performing microservices, APIs, solution architecting and general back end engineering 

### personal

even though my name sounds Russian, I'm actually from [Bulgaria](https://en.wikipedia.org/wiki/Bulgaria). I was born there and I came to the US when I was 15 years old, back in 2007 

I graduated college in 2014 at [University of Kentucky](http://www.uky.edu) with a bachelor's in Computer Science and minor in Mathematics. I love developing software, playing video games, spending time with friends, family and being active

### hobbies

- software research/personal projects - one of my favorite forum is [hacker news](https://news.ycombinator.com/)
- playing video games - it's only fun when playing with friends
- spending time with family and friends
- reading about all sorts of financial stuff and achieving financial independence
- being active; sports, hiking, walks, cycling and more
